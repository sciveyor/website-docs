---
title: Performing Searches
type: docs
weight: 20
---

You will need to search through the article database in order to create
datasets. You can either do this as part of the [normal analysis
workflow,]({{<ref "workflow.md">}}) or by visiting the "Browse/Search Database"
link under "Advanced Tools" in the top menu bar.

## Basic Query Syntax

The default search bar in Sciveyor uses a complex search query parser that
allows users to construct all kinds of searches. Here's some examples:

- `fish`

  A basic, default search looks for the terms provided, both as a phrase and as
  individual terms, in the titles of authors, the article full text, and the
  journals in which they are published (in that order of priority). A basic
  search for `fish`, therefore, would return articles with `fish` in the title,
  with `fish` mentioned in the full text, and published in any journals whose
  titles contain `fish`.

- `author:Simpson`

  You can also search for terms within particular metadata fields. The following
  fields can be used:

  - `fulltext`: The article's full text
  - `authors` (or `author`, or `au`): The authors of the article
  - `title`: The title of the article
  - `journal`: The journal in which the article was published
  - `year`: The year in which the article was published
  - `volume`: The volume of the journal in which the article appears
  - `number`: The issue number of the journal in which the article appears
  - `pages`: The pages of the journal in which the article appears
  - `doi`: The Digital Object Identifier (DOI)
  - `license`: The license for the article

  To search by metadata fields, type a field and a search term separated by a
  colon. To search for a phrase that includes spaces, surround the phrase with
  double-quotes:

  `author:"John Thompson"`

  These metadata queries can be combined using boolean operators.

- `author:Thompson OR journal:Fish`

  The boolean combiners `AND` and `OR` can be used. The default combination is
  `AND`.

(This search parser is an `edismax` parser, described in
[the Solr documentation.](https://wiki.apache.org/solr/ExtendedDisMax))

## Creating Datasets

You can save the current search results as a dataset by clicking the green "Save
Results" button. For more information, see the section of the manual [on the
analysis workflow,]({{<ref "workflow.md">}}) or [information on advanced dataset
management.]({{<ref "datasets.md">}})

## Search Filters

The right-hand column on the search page allows you to filter the search results
by author, journal, or decade of publication. Once these filters are added to
the search, you will only see results that satisfy all of the active filters.
The filters appear in a bar just beneath the search query. Clicking on them will
remove them, or you can click on the Remove All button to reset all filters.

## Document Information

To get more information about a particular document, click the "More" button at
the right of the citation. You'll find a variety of tools. You can either create
a dataset containing only a single document, or you can add this document to a
dataset that you've already created. You can visit the website of the journal to
download a PDF of the article, if you have access to the content. You can search
for the article using your local libraries ([see the user accounts page for more
information]({{<ref "accounts.md">}})), or with the WorldCat or UK OpenURL
resolvers. You can look for information about this article on external sources,
such as Google Scholar or CiteULike. Lastly, you can see detailed information
about the article, including the terms of our license for its content and its
DOI.

## Advanced Searches

The advanced search page lets you carefully build a search query, combining
metadata searches for the various fields with a user-friendly search form. It
can be reached via a button at the bottom of the right-hand panel on the search
page.

Three fields are slightly different from the basic search parser. The document
full text, the article, and the journal title can either be searched "fuzzy" or
"exact." The exact search requires the exact words that you type to be present
in the listed field. Fuzzy searches perform
[stemming](https://en.wikipedia.org/wiki/Stemming) on both the words in your
query and the words in the field, so that varying word endings and forms will
match. (Both the default Sciveyor search queries and most search engines such as
Google use fuzzy or stemmed searching.)

Also, if you select either the "Authors" field or the "Journal (exact)" field,
Sciveyor will auto-complete values from the database as you begin to type.

## Solr Query Syntax

Lastly, if you visit the advanced search page and click on the bottom link to
search in Solr's query syntax, you can use the full power of the Solr search
server. We use Solr's
[Lucene query syntax,](https://wiki.apache.org/solr/SolrQuerySyntax) and the
full list of fields that can be queried is provided up above.
