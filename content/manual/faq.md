---
title: FAQ
type: docs
weight: 5
description: Answers to some of our most frequently asked questions.
---

## What happened to evoText and RLetters?

Quick answer: both evoText and RLetters are now known as **Sciveyor.**

See the [history page]({{<relref "history.md">}}) for more information about the
history of the project, including why we decided to combine and rename these two
former efforts.

## I selected my language in the user preferences, but all the text is still in English

The list of languages indicates all the languages in which it is possible to
translate the application. For most of these languages, the bulk of the
translation work has not yet been done. If you'd like to contribute translations
in your native language, we'd love to have them: please visit the
[contributing translations page](FIXME) at the website for more information
about how you can help.

## Does Sciveyor use cookies?

Yes, but only to store a very small amount of data about whether you're logged
in and to enable the "Remember me" function for logins. You can visit a page
containing information about cookie usage in two ways:

1.  Click "Help", then "User data" in the navigation bar. There should be a link
    to the cookie policy on that page.
2.  Clear your browser's cookies, reload the site, and select "More information"
    in the cookie warning which appears.

