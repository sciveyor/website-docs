---
title: Contributing Translations
weight: 5
---

Would you like the basic interface of Sciveyor to be translated into your native
language? So would we! We've done our best to make it possible to quickly and
easily translate Sciveyor into other languages. Our translations are also stored
at Codeberg, on their [hosted Weblate software.](https://translate.codeberg.org)

If you want to help out, head over to Weblate, create an account, [and visit the
Sciveyor project page.](https://translate.codeberg.org/projects/sciveyor/) More documentation is available there!

Thanks for your help -- we're excited to make Sciveyor accessible to as many
people as possible!
