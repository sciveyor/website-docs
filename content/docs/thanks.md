---
title: Thanks
weight: 12
description: >
  Thanks and acknowledgments to developers and funders who have made Sciveyor
  possible.
---

Special thanks to all contributors to the code. In addition to the
[list of contributors on Codeberg,](https://codeberg.org/sciveyor/sciveyor/activity)
thanks as well to [rrrene](https://github.com/rrrene) and
[etahnsr](https://github.com/ethansr) who contributed over on GitHub.

We also have received the help of a great community of translators at
[Transifex.](https://www.transifex.com/cpence/sciveyor/) Thanks especially to
Alejandro León Aznar.

Also, several features of Sciveyor wouldn't be possible without the excellent
work of other open-source programmers. Thanks in particular to those behind
[RSolr](https://github.com/mwmitchell/rsolr) and
[RSolr::Ext](https://github.com/mwmitchell/rsolr-ext), and
[bibtex-ruby](https://github.com/inukshuk/bibtex-ruby).

Charles Pence was supported in the development of Sciveyor by the
[Fonds de la Recherche Scientifique - FNRS](https://www.frs-fnrs.be/) under
grant no. F.4526.19. For the previous versions of Sciveyor (evoText and
RLetters), Charles Pence and Grant Ramsey were supported by the
[National Science Foundation](http://www.nsf.gov), HPS Scholars Award grant no.
1456573, and the
[National Evolutionary Synthesis Center (NESCent),](http://www.nescent.org) NSF
grant no. EF-0905606.

[![FNRS](/images/fnrs.png)](https://frs-fnrs.be)
[![National Science Foundation](/images/nsf.gif)](https://nsf.gov)
[![National Evolutionary Synthesis Ceter](/images/nescent.png)](https://nescent.org)

## Copyright

Sciveyor &copy; 2011–2021 [Charles Pence](mailto:charles@charlespence.net).
Sciveyor is licensed under the [MIT license.](https://mit-license.org/)

The stop lists found in `app/lib/sciveyor/analysis/stop_list` are released under
the BSD license by the Apache Solr project. The colors in
`app/lib/sciveyor/visualization/color_brewer` are released by
[the ColorBrewer project](http://www.colorbrewer.org) under the Apache license.
