---
title: Privacy Policy
type: docs
weight: 900
description: The Sciveyor privacy policy.
---

## The short version

We use internet-standard encryption to store your username and password with us.
Only the encrypted version of your password is ever stored. Even so, we
recommend you use a unique password for this website -- just like you should
with every website!

Your saved datasets and analysis results are only visible to your user account,
and to the server administrators. Administrators would likely only need to look
at analysis tasks when something goes wrong.

We will only ever send you e-mails when your analysis jobs finish. We hate spam,
and we know that you do, too.

## The long, detailed version

Effective 2013/10/14

### Our commitment to privacy

Your privacy is important to us. To better protect your privacy, we provide this
notice explaining our online information practices and the choices you can make
about the way your information is collected and used. To make this notice easy
to find, we make it available on our homepage and at every point where
personally identifiable information may be requested.

### The information we collect

This notice applies to all information collected or submitted on any Sciveyor
website (as configured by default). You may have come from a website which has
altered this default behavior. If so, the authors of Sciveyor cannot be held
responsible for these changes in behavior.

On some pages, you can create datasets, save those datasets, analyze them, and
register to receive materials. The types of personal information collected at
these pages are names, passwords, and e-mail addresses.

### The way we use information

We use the information you provide about yourself only for two purposes: (1) to
identify the users which own a given saved dataset, and (2) to send users e-mail
regarding the completion of deferred jobs. We do not, nor will we ever, share
this information with outside parties.

The only e-mail you will ever receive from Sciveyor is an e-mail indicating that
a deferred job (an analysis that requires the user to wait until it is finished,
then return to download the results) has finished and is available for download.

You must register with our website in order to save your datasets and perform
analyses. Without this registration, we would be unable to know who owned each
dataset in our system.

We use non-identifying and aggregate information to better design our website.
For example, we may keep track of the fact that X number of individuals visited
a certain area on our website, in order to redesign a particular page. We will
not disclose this information to any third parties, nor can any portion of it be
used to identify our users.

Finally, should these terms change, we will never user or share the information
you provide to us in ways other than the ones outlined above without giving you
the opportunity to opt-out, cancel your account, or otherwise prohibit such
unrelated uses.

### Our commitment to data security

To prevent unauthorized access, maintain data accuracy, and ensure the correct
use of information, we have put in place appropriate physical, electronic, and
managerial procedures to safeguard and secure the information we collect online.

When you log in to Sciveyor, you create a user account with a password. This
password is encrypted before being stored in the database. The code which
manages this encryption and storage is the
[Devise](https://github.com/plataformatec/devise) user authentication system,
which is used by hundreds of websites across the internet, and is audited for
security on a regular basis. We strive to keep current on security patches and
other measures we can take to prevent attackers from obtaining unauthorized
access to our servers.

### Our commitment to children's privacy

In compliance with government regulations, we never collect or maintain
information at our website from those we know to be under 13 years of age, and
no part of our website is structured to attract anyone under 13.

### How you can access or correct your information

Users may change any of their information stored at any time, including
cancelling their accounts, by visiting their user page at Sciveyor.

### How to contact us

Should you have other questions or concerns about these privacy policies, please
send the Sciveyor development team an e-mail at <charles@charlespence.net>, or
contact the developers of the website you were using directly.
