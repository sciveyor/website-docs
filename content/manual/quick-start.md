---
title: Quick Start
type: docs
weight: 2
description: The shortest possible tutorial for getting started with Sciveyor.
---

If you're impatient to get started as quickly as you can, here's the briefest
set of steps that can get you to creating a datasets and viewing the results of
an analysis. If you want to learn more, your next stop should be the
[tutorial]({{< ref "tutorial.md" >}}), and then on to the rest of the
documentation in the menu at left!

1.  Sign up for a new user account, and log in.
2.  From your dashboard page, click the button to start a new analysis.
3.  Select the question, "When were a given set of articles published?"
4.  Read the information about the analysis, and click Start.
5.  Click the button to create another dataset.
6.  Click "Save Results" next to the search bar.
7.  Give this dataset a name, and click "Create Dataset."
8.  Click "Set Job Options."
9.  Click "Start Analysis Job."
10. After waiting for the job to finish, visit the dashboard and click the
    button to see the results of your old analyses.
11. In the list, click "View" next to your newly finished job.
12. You should see a graph showing the publication dates of all of the articles
    available in the database.
13. Congrats! You just ran your first analysis! Read on for more information
    about each of those steps.
