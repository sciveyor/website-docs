---
title: Code Documentation
linkTitle: Code Documentation
weight: 1
description: >
  The documentation for the Ruby source to the Sciveyor client.
---

The source code documentation for the
[Sciveyor web application,](https://codeberg.org/sciveyor/sciveyor) generated
directly from its Ruby source code, can be [read online here.](/sciveyor-yard/)
